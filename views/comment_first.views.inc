<?php

/**
 * @file
 * Views integration with First Comment.
 */

 /**
  * Implements hook_views_data_alter().
  */
function comment_first_views_data_alter(&$data) {

  $data['node_comment_statistics']['first_cid'] = array(
    'title' => t('First comment CID'),
    'help' => t('Display the first comment of a node'),
    'relationship' => array(
      'title' => t('First Comment'),
      'help' => t('The first comment of a node.'),
      'group' => t('Comment'),
      'base' => 'comment',
      'base field' => 'cid',
      'handler' => 'views_handler_relationship',
      'label' => t('First Comment'),
    ),
  );

}
