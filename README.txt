Keeps track of first comment attached to content. Also adds support for referencing to the first comment in views.
First comment can be referenced in views by adding relationship of the first comment.


 -- REQUIREMENTS --
 
Comment module(Core).


Author
------
Elias Tertsunen (Grip Studios Interactive Oy)
elias@gripstudios.com